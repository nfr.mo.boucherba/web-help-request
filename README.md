# Web-Help-Request

Web Help Request

# Contexte du projet
Rien ne va plus dans la promo, le formateur ne sait plus où donner de la tête, il explique mal ses briefs projet et tout le monde demande de l'aide. Il n'arrive plus à savoir qui a besoin d'aide et quand. Aidez le à répondre à ce problème en réalisant une interface front end qui permet de distribuer des tickets aux apprenants de sa promo.

L'objectif est de s'inspirer de la maquette proposée en ressource ainsi que l'API Rest (voir la documentation de l'API) développée grâce au framework Express afin de permettre via le front end de :

Créer des tickets en base de données
Lister les tickets en cours (ceux qui ne sont pas résolus)
Mettre à jour des tickets (une fois que la question est répondue ^^)
En bonus, l'interface front devra permettre de :

Gérer les apprenants dans la base de données
Faire des statistiques sur les demandes (Par exemple : quel apprenant fait le plus de tickets ?)

# Modalités pédagogiques
Vous réaliserez ce projet en groupe de 2 personnes.

# Critères de performance
La création de ticket fonctionne
La Liste des tickets en cours non résolus fonctionne correctement
La mise à jour des tickets fonctionne
La sémantique HTML et l’accessibilité  est respectée
Le code est indenté, commenté et propre(pas de console.log qui traîne etc)
Le site est développé avec Bootstrap 5
Le site est adaptatif (responsive desktop / mobile)
Le site est en ligne
Les commits sont réguliers et explicites

# Modalités d'évaluation
Évaluation avec le formateur

# Livrables
Depot gitlab
Site en ligne

# Template Webpack Typescript

## Requirements

[Node.js](https://nodejs.org) is required to install dependencies and run scripts via `npm`.

## Writing Code

Download the repo, **DON'T CLONE** it, run `npm install` from your project directory. Then, you can start the local development
server by running `npm start`.

After starting the development server with `npm start`, you can edit any files in the `src` folder
and webpack will automatically recompile and reload your server (available at `http://localhost:8080`
by default).

## Available Commands

| Command | Description |
|---------|-------------|
| `npm install` | Install project dependencies |
| `npm start` | Build project and open web server running project |
| `npm run build` | Builds code bundle with production settings (minification, uglification, etc..) |

const title = 'Distributeur de tickets pour la promo' as string;
document.getElementById('content')!.innerHTML = `${title}`;

let sendTicket = document.getElementById('sendTicket');
let ticketSubject = document.getElementById('ticketSubject')
let ticketList = document.getElementById('ticketList')
let refresh = document.getElementById('refresh')
let postUser = document.getElementById('postUser');
let userAdd = document.getElementById('userAdd');
let userNameInput = document.getElementById('userNameInput');
let userPasswordInput = document.getElementById('userPasswordInput');
let NextTicket = document.getElementById('nextT')
let currentTicket = document.getElementById('currentT')

refresh.addEventListener("click", getTicket)
sendTicket.addEventListener("click", addTicket)
NextTicket.addEventListener("click", setTicket)


function addTicket() {
    //@ts-ignore
    let name = postUser.value
    //@ts-ignore
    let content = ticketSubject.value
    let id: number

    fetch('https://web-help-request-api.herokuapp.com/users').then((response) =>
        response.json().then((data) => {
            data.data.forEach((element: any) => {
                if (name == element.username) {
                    id = element.id
                }
            })
            fetch('https://web-help-request-api.herokuapp.com/tickets', {
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                body: new URLSearchParams({ userId: id.toString(), subject: content })
            })
        }))
    getTicket()
}

function getTicket() {
    let users: any = []
    fetch('https://web-help-request-api.herokuapp.com/users').then((response) =>
        response.json().then((data) => {
            data.data.forEach((element: any) => {
                users.push(element.username)
            })
            fetch('https://web-help-request-api.herokuapp.com/tickets').then((response) =>

                response.json().then((data) => {
                    ticketList.innerHTML = ''
                    data.data.forEach((element: any) => {
                        ticketList.innerHTML += `<div class="taoublierdeegale">ticket de ${users[element.users_id - 1]} <br> sujet: ${element.subject} <br><br></div>`
                    });
                }))
        }))
} getTicket()

function getTheOneTicket() {
    let i = 0
    let users: any = []

    fetch('https://web-help-request-api.herokuapp.com/users').then((response) =>
        response.json().then((data) => {
            data.data.forEach((element: any) => {
                users.push(element.username)
            })
            fetch('https://web-help-request-api.herokuapp.com/tickets').then((response) =>
                response.json().then((data) => {
                    currentTicket.innerHTML = `ticket de ${users[data.data[i].users_id - 1]} <br> sujet: ${data.data[i].subject} <br><br>`
                }))
        }))

} getTheOneTicket()

function setTicket() {
    let i = 0
    let users: any = []

    fetch('https://web-help-request-api.herokuapp.com/users').then((response) =>
        response.json().then((data) => {
            data.data.forEach((element: any) => {
                users.push(element.username)
            })
            fetch('https://web-help-request-api.herokuapp.com/tickets').then((response) =>
                response.json().then((data) => {
                    fetch(`https://web-help-request-api.herokuapp.com/tickets/${data.data[i].id}`, {
                        method: 'PATCH'
                    })
                    getTicket()
                    i++
                    currentTicket.innerHTML = `ticket de ${users[data.data[i].users_id - 1]} <br> sujet: ${data.data[i].subject} <br><br>`
                }))
        }))

}

function getUsers() {
    fetch(`https://web-help-request-api.herokuapp.com/users`)
        .then(resp => resp.json())
        .then(data => {
            postUser.innerHTML = `<div class="loupapeur"><option selected>Select User</option></div>`;
            data.data.forEach((e: any) => {
                postUser.innerHTML += `<div class="loupapeur"><option id="${e.id}">${e.username}</option></div>`
            });
        });
};
getUsers();

userAdd.addEventListener("click", addUsers)

function addUsers() {

    console.log();
    //@ts-ignore
    const mo = userNameInput.value;
    //@ts-ignore
    const lo = userPasswordInput.value;
    fetch(`https://web-help-request-api.herokuapp.com/users`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({ username: mo, password: lo })
    });
};